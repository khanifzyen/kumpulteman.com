<?php
return [
    'adminEmail' => 'khanif.zyen@gmail.com',
    'supportEmail' => 'khanif.zyen@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
    // set target language to be Indonesian
    'language' => 'id-ID',
    
    // set source language to be Indonesian
    'sourceLanguage' => 'id-ID',
];
