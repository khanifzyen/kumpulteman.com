<?php
namespace common\components;
 
use yii\rbac\Rule;
use common\models\Profile;
 
class ProfileOwner extends Rule
{
    public function execute($user, $item, $params)
    {
        // paramater $params dikirim dari AccessControl dengan nilai dari $_GET
        //$model = Profile::findOne($params['id']);
        $model = Profile::find()->where(['id_user'=>$params['id']])->one();
        // var_dump($model && $user == $model->id_user);
        // exit;
        return $model && $user == $model->id_user;
    }
}