<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%agenda}}".
 *
 * @property integer $id
 * @property string $judul
 * @property string $isi
 * @property string $tanggal_pelaksanaan
 * @property integer $donasi
 * @property string $terakhir_donasi
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $updatedBy
 * @property User $createdBy
 * @property AgendaAngkatan[] $agendaAngkatans
 * @property AgendaDonasiAdmin[] $agendaDonasiAdmins
 * @property AgendaDonasiData[] $agendaDonasiDatas
 * @property KomentarAgenda[] $komentarAgendas
 */
class Agenda extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agenda}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'isi', 'tanggal_pelaksanaan', 'terakhir_donasi', 'created_by', 'updated_by'], 'required'],
            [['isi'], 'string'],
            [['tanggal_pelaksanaan', 'terakhir_donasi'], 'safe'],
            [['donasi', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['judul'], 'string', 'max' => 255],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'judul' => Yii::t('app', 'Judul'),
            'isi' => Yii::t('app', 'Isi'),
            'tanggal_pelaksanaan' => Yii::t('app', 'Tanggal Pelaksanaan'),
            'donasi' => Yii::t('app', 'Donasi'),
            'terakhir_donasi' => Yii::t('app', 'Terakhir Donasi'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendaAngkatans()
    {
        return $this->hasMany(AgendaAngkatan::className(), ['id_agenda' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendaDonasiAdmins()
    {
        return $this->hasMany(AgendaDonasiAdmin::className(), ['id_agenda' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendaDonasiDatas()
    {
        return $this->hasMany(AgendaDonasiData::className(), ['id_agenda' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKomentarAgendas()
    {
        return $this->hasMany(KomentarAgenda::className(), ['id_agenda' => 'id']);
    }
}
