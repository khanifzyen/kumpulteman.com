<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%komentar_foto}}".
 *
 * @property integer $id
 * @property integer $id_foto_profile
 * @property string $isi
 * @property integer $id_user
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $updatedBy
 * @property User $createdBy
 * @property FotoProfile $idFotoProfile
 * @property User $idUser
 */
class KomentarFoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%komentar_foto}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_foto_profile', 'isi', 'id_user', 'created_by', 'updated_by'], 'required'],
            [['id_foto_profile', 'id_user', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['isi'], 'string'],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['id_foto_profile'], 'exist', 'skipOnError' => true, 'targetClass' => FotoProfile::className(), 'targetAttribute' => ['id_foto_profile' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_foto_profile' => Yii::t('app', 'Id Foto Profile'),
            'isi' => Yii::t('app', 'Isi'),
            'id_user' => Yii::t('app', 'Id User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFotoProfile()
    {
        return $this->hasOne(FotoProfile::className(), ['id' => 'id_foto_profile']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
