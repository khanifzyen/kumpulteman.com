<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AgendaDonasiDataBukti;

/**
 * AgendaDonasiDataBuktiSearch represents the model behind the search form about `common\models\AgendaDonasiDataBukti`.
 */
class AgendaDonasiDataBuktiSearch extends AgendaDonasiDataBukti
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_agenda_donasi_data', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['upload_bukti'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgendaDonasiDataBukti::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_agenda_donasi_data' => $this->id_agenda_donasi_data,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'upload_bukti', $this->upload_bukti]);

        return $dataProvider;
    }
}
