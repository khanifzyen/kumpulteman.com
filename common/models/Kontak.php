<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%kontak}}".
 *
 * @property integer $id
 * @property string $nama
 * @property string $email
 * @property string $judul
 * @property string $isi
 * @property string $link_url
 * @property string $file_capture
 */
class Kontak extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%kontak}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'email', 'judul', 'isi'], 'required'],
            [['isi'], 'string'],
            [['nama', 'email', 'judul', 'link_url', 'file_capture'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'email' => Yii::t('app', 'Email'),
            'judul' => Yii::t('app', 'Judul'),
            'isi' => Yii::t('app', 'Isi'),
            'link_url' => Yii::t('app', 'Link Url'),
            'file_capture' => Yii::t('app', 'File Capture'),
        ];
    }
}
