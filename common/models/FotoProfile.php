<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%foto_profile}}".
 *
 * @property integer $id
 * @property integer $id_profile
 * @property string $file_asli
 * @property string $thumb_depan
 * @property integer $is_profile_pic
 * @property integer $like_this
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $updatedBy
 * @property User $createdBy
 * @property Profile $idProfile
 * @property KomentarFoto[] $komentarFotos
 */
class FotoProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%foto_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_profile', 'file_asli', 'thumb_depan', 'created_by', 'updated_by'], 'required'],
            [['id_profile', 'is_profile_pic', 'like_this', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['file_asli', 'thumb_depan'], 'string', 'max' => 255],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['id_profile'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['id_profile' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_profile' => Yii::t('app', 'Id Profile'),
            'file_asli' => Yii::t('app', 'File Asli'),
            'thumb_depan' => Yii::t('app', 'Thumb Depan'),
            'is_profile_pic' => Yii::t('app', 'Is Profile Pic'),
            'like_this' => Yii::t('app', 'Like This'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'id_profile']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKomentarFotos()
    {
        return $this->hasMany(KomentarFoto::className(), ['id_foto_profile' => 'id']);
    }
}
