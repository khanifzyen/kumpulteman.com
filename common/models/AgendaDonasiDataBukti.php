<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%agenda_donasi_data_bukti}}".
 *
 * @property integer $id
 * @property integer $id_agenda_donasi_data
 * @property string $upload_bukti
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $updatedBy
 * @property User $createdBy
 * @property AgendaDonasiData $idAgendaDonasiData
 */
class AgendaDonasiDataBukti extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agenda_donasi_data_bukti}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_agenda_donasi_data', 'upload_bukti', 'created_by', 'updated_by'], 'required'],
            [['id_agenda_donasi_data', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['upload_bukti'], 'string', 'max' => 255],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['id_agenda_donasi_data'], 'exist', 'skipOnError' => true, 'targetClass' => AgendaDonasiData::className(), 'targetAttribute' => ['id_agenda_donasi_data' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_agenda_donasi_data' => Yii::t('app', 'Id Agenda Donasi Data'),
            'upload_bukti' => Yii::t('app', 'Upload Bukti'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAgendaDonasiData()
    {
        return $this->hasOne(AgendaDonasiData::className(), ['id' => 'id_agenda_donasi_data']);
    }
}
