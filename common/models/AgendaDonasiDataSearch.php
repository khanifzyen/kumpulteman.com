<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AgendaDonasiData;

/**
 * AgendaDonasiDataSearch represents the model behind the search form about `common\models\AgendaDonasiData`.
 */
class AgendaDonasiDataSearch extends AgendaDonasiData
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_agenda', 'id_user', 'nominal', 'validasi', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgendaDonasiData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_agenda' => $this->id_agenda,
            'id_user' => $this->id_user,
            'nominal' => $this->nominal,
            'validasi' => $this->validasi,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
