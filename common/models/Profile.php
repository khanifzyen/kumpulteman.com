<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%profile}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $nama_panggilan
 * @property string $nama_lengkap
 * @property string $alamat_asal
 * @property string $alamat_sekarang
 * @property integer $angkatan
 * @property integer $id_kelas
 * @property string $pesan_kesan
 * @property string $pekerjaan
 * @property string $no_hp_wa
 * @property string $facebook
 * @property string $instragram
 * @property string $twitter
 * @property string $path
 * @property string $youtube
 * @property string $folder_foto
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property FotoProfile[] $fotoProfiles
 * @property User $updatedBy
 * @property User $createdBy
 * @property Kelas $idKelas
 * @property User $idUser
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user', 'angkatan', 'id_kelas', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['pesan_kesan'], 'string'],
            [['nama_panggilan', 'nama_lengkap', 'alamat_asal', 'alamat_sekarang', 'pekerjaan', 'facebook', 'instragram', 'twitter', 'path', 'youtube', 'folder_foto'], 'string', 'max' => 255],
            [['no_hp_wa'], 'string', 'max' => 20],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['id_kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['id_kelas' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'nama_panggilan' => Yii::t('app', 'Nama Panggilan'),
            'nama_lengkap' => Yii::t('app', 'Nama Lengkap'),
            'alamat_asal' => Yii::t('app', 'Alamat Asal'),
            'alamat_sekarang' => Yii::t('app', 'Alamat Sekarang'),
            'angkatan' => Yii::t('app', 'Angkatan'),
            'id_kelas' => Yii::t('app', 'Id Kelas'),
            'pesan_kesan' => Yii::t('app', 'Pesan Kesan'),
            'pekerjaan' => Yii::t('app', 'Pekerjaan'),
            'no_hp_wa' => Yii::t('app', 'No Hp Wa'),
            'facebook' => Yii::t('app', 'Facebook'),
            'instragram' => Yii::t('app', 'Instragram'),
            'twitter' => Yii::t('app', 'Twitter'),
            'path' => Yii::t('app', 'Path'),
            'youtube' => Yii::t('app', 'Youtube'),
            'folder_foto' => Yii::t('app', 'Folder Foto'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFotoProfiles()
    {
        return $this->hasMany(FotoProfile::className(), ['id_profile' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKelas()
    {
        return $this->hasOne(Kelas::className(), ['id' => 'id_kelas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
