<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%komentar_berita}}".
 *
 * @property integer $id
 * @property integer $id_berita
 * @property integer $id_user
 * @property string $isi
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $updatedBy
 * @property User $createdBy
 * @property Berita $idBerita
 * @property User $idUser
 */
class KomentarBerita extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%komentar_berita}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_berita', 'id_user', 'isi', 'created_by', 'updated_by'], 'required'],
            [['id_berita', 'id_user', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['isi'], 'string'],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['id_berita'], 'exist', 'skipOnError' => true, 'targetClass' => Berita::className(), 'targetAttribute' => ['id_berita' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_berita' => Yii::t('app', 'Id Berita'),
            'id_user' => Yii::t('app', 'Id User'),
            'isi' => Yii::t('app', 'Isi'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBerita()
    {
        return $this->hasOne(Berita::className(), ['id' => 'id_berita']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
