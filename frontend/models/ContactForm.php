<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Kontak;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Kode verifikasi',
            'name' =>'Nama',
            'subject'=>'Judul',
            'body'=>'Isi'
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }
    
    /**
     * Save form Contacts to tbl_kontak.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function saveToKontak(){
        $kontak = new Kontak();
        $kontak->nama = $this->name;
        $kontak->email = $this->email;
        $kontak->judul = $this->subject;
        $kontak->isi = $this->body;
        if ($kontak->save() && $kontak->validate())
            return true;
        else 
            return false;
    }
}
