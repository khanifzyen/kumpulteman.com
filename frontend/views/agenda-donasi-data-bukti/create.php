<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AgendaDonasiDataBukti */

$this->title = Yii::t('app', 'Create Agenda Donasi Data Bukti');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agenda Donasi Data Buktis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agenda-donasi-data-bukti-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
