<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\KomentarFoto */

$this->title = Yii::t('app', 'Create Komentar Foto');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Komentar Fotos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komentar-foto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
