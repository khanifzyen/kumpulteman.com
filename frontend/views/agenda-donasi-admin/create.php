<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AgendaDonasiAdmin */

$this->title = Yii::t('app', 'Create Agenda Donasi Admin');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agenda Donasi Admins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agenda-donasi-admin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
