<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\KomentarBerita */

$this->title = Yii::t('app', 'Create Komentar Berita');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Komentar Beritas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komentar-berita-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
