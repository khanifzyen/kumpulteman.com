<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AgendaAngkatan */

$this->title = Yii::t('app', 'Create Agenda Angkatan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agenda Angkatans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agenda-angkatan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
