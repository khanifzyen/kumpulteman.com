<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\KomentarAgenda */

$this->title = Yii::t('app', 'Create Komentar Agenda');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Komentar Agendas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komentar-agenda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
