<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AgendaPeserta */

$this->title = Yii::t('app', 'Create Agenda Peserta');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agenda Pesertas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agenda-peserta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
