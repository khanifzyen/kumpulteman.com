<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FotoProfile */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Foto Profile',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Foto Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="foto-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
