<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FotoProfile */

$this->title = Yii::t('app', 'Create Foto Profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Foto Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="foto-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
