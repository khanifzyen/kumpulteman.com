<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Kumpul Teman - Jalin Silaturahmi dengan Teman Sekolahmu ';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Selamat Datang!</h1>

        <p class="lead">Mari kita jalin silaturahmi sesama teman sekolah yang terpisah puluhan tahun. Semua Angkatan.</p>

        <p><?= Html::a('Daftar Alumni Sekarang!', ['site/signup/'], ['class' => 'btn btn-lg btn-success']) ?></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
            
                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>Agenda #1</strong></div>
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                            <span class="badge">Reuni Angkatan 2004</span>
                            Kegiatan
                            </li>
                            <li class="list-group-item">
                            <span class="badge">1 Desember 2017</span>
                            Pelaksanaan
                            </li>
                            <li class="list-group-item">
                            <span class="badge">20</span>
                            Follower
                            </li>
                            <li class="list-group-item">
                            <span class="badge">20</span>
                            Komentar
                            </li>
                            <li class="list-group-item">
                            <span class="badge">14 Oktober 17 12:31</span>
                            Aktivitas Terakhir
                            </li>
                        </ul>
                        <a class="btn btn-default" href="#">Selengkapnya</a>
                    </div>
                </div>
                
                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>Agenda #2</strong></div>
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                            <span class="badge">Reuni Angkatan 1980</span>
                            Kegiatan
                            </li>
                            <li class="list-group-item">
                            <span class="badge">1 Januari 2018</span>
                            Pelaksanaan
                            </li>
                            <li class="list-group-item">
                            <span class="badge">20</span>
                            Follower
                            </li>
                            <li class="list-group-item">
                            <span class="badge">20</span>
                            Komentar
                            </li>
                            <li class="list-group-item">
                            <span class="badge">14 Oktober 17 12:31</span>
                            Aktivitas Terakhir
                            </li>
                        </ul>
                        <a class="btn btn-default" href="#">Selengkapnya</a>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>Alumni Terbaru</strong></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                  <img src="/img/man_in_hat.png" alt="...">
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                  <img src="/img/man_in_hat.png" alt="...">
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                  <img src="/img/man_in_hat.png" alt="...">
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="#" class="thumbnail">
                                  <img src="/img/man_in_hat.png" alt="...">
                                </a>
                            </div>
                          </div>
                        <p><a class="btn btn-default" href="#">Selengkapnya &raquo;</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>Berita Terbaru</strong></div>
                    <div class="panel-body">
                        <h3>Launching kumpulteman.com</h3>
                        <h6 class="label label-primary">Khanif, Sab 14 Okt 2017 09:27</h6>
                        <p>Pada Sabtu tanggal 14 Oktober 2017, berlokasi di internet, telah dilaunching kumpulteman.com. Kumpulteman merupakan platform yang 
                        bisa digunakan oleh alumni sekolah, sehingga bisa memudahkan mereka berkumpul dan bertemu dalam sebuah reuni
                        setelah berpisah selama puluhan tahun. Dan itulah yang menjadi prinsip asal dari berdirinya kumpulteman.com</p>
        
                        <p><a class="btn btn-default" href="#">Selengkapnya &raquo;</a></p>
                        <hr />
                        
                        <p><a class="btn btn-default" href="#">Index Semua berita &raquo;</a></p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
