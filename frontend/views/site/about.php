<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Tentang Kami';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    
    <div class="panel panel-primary">
      <div class="panel-heading"><strong><?= Html::encode($this->title) ?></strong></div>
      
      <div class="panel-body">
          <p class="text-center">
          <img src="\img\kumpulteman-s.png">
          </p>
        <p>kumpulteman.com adalah platform yang dikhususkan bagi anda yang ingin bernostalgia dengan teman-teman sekolah. Versi alpha adalah versi yang masih dalam tahap pengembangan yang masif, sehingga seluruh fitur belum bisa dinikmati. Kami butuh respon dari anda sebagai pemakai kumpulteman.</p>
        <p>Fitur-fitur yang akan hadir dalam platform kumpulteman adalah:</p>
        <ol>
            <li> Mendaftar akun sebagai alumni
            <li> Mengisi profile identitas dan memilih sekolah, kelas dan tahun angkatan
            <li> Mengupload foto untuk profile identitas anda
            <li> Hak akses alumni yang hanya bisa menemukan alumni lain dalam satu sekolah, satu angkatan atau beberapa angkatan. Tidak diijinkan untuk melihat profile alumni sekolah lain
            <li> Mendaftar ke kumpulteman untuk menjadi adminangkatan
            <li> Membuat agenda (reuni) yang dilakukan oleh adminangkatan. Agenda bisa hanya khusus untuk satu angkatan, atau beberapa angkatan tertentu
            <li> Alumni yang terdaftar bisa melihat agenda (reuni) yang terdaftar di kumpulteman
            <li> Alumni bisa menyatakan hadir dalam agenda (reuni), atau dalam istilah kumpulteman adalah mem-follow agenda
            <li> Alumni hanya bisa follow agenda, yang sudah di-set hak aksesnya oleh adminangkatan yaitu dengan membatasi hanya angkatan berapa saja yang bisa ikut agenda (reuni) tersebut.
            <li> Alumni yang sudah follow agenda, bisa memberikan komentar ke agenda tersebut
            <li> adminangkatan bisa membuka donasi untuk agenda (reuni). Adapun adminangkatan bisa menunjuk beberapa alumni sebagai admindonasi yang mengkoordinir donasi yang akan dibayarkan oleh alumni lain dalam rangka salah satu syarat untuk mengikuti suatu agenda (reuni)
            <li> Alumni bisa memberikan donasi (posting donasi) dan mengupload bukti transfer, untuk kemudian akan divalidasi oleh admindonasi.
            <li> Donasi bisa dilihat oleh semua alumni yang mem-follow agenda (reuni) tersebut sebagai bentuk transparansi adminangkatan dan admindonasi yang bertindak sebagai panitia agenda (reuni)
        </ol>
        <p>Adapun kumpulteman sama sekali tidak mengambil uang sepeser pun dari donasi yang dibuka oleh adminangkatan. Segala bentuk kehilangan/kerugian bukan tanggung jawab kumpulteman. Semoga website ini menjadi berkah dan manfaat bagi kita semua. Amin...</p>
        <p> Salam Admin</p>
        <hr />
        <p>Changelog</p>
        <strong>Alpha 1 : 14 Oktober 2017</strong>
        <ol>
            <li>Rilis pertama kali di kumpulteman.com</li>
            <li>Register sebagai alumni</li>
        </ol>          
      </div>
    </div>


</div>
