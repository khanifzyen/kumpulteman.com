<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108151216-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-108151216-1');
    </script>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="description" content="kumpultemangi.com adalah platform yang dikhususkan bagi anda yang ingin bernostalgia dengan teman-teman sekolah. Versi alpha adalah versi yang masih dalam tahap pengembangan yang masif, sehingga seluruh fitur belum bisa dinikmati. Kami butuh respon dari anda sebagai pemakai kumpulteman.Free Web tutorials">
    <meta name="keywords" content="kumpulteman, alumni, reuni, sahabat, kawan, teman, sekolah, angkatan">
    <meta name="author" content="Akhmad Khanif Zyen">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Kumpul Teman (versi alpha)',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Beranda', 'url' => ['/site/index']],
        ['label' => 'Tentang Kami', 'url' => ['/site/about']],
        ['label' => 'Kontak Kami', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = ['label'=>'Agenda','url'=>['/site/agenda']];
        $menuItems[] = ['label'=>'Alumni','url'=>['/profile/index']];
        $menuItems[] = ['label'=>'Profile','url'=>['/profile/update-own-profile','id'=> Yii::$app->user->id]];
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Kumpul Teman <?= date('Y') ?></p>

        <p class="pull-right">Created by <?= Html::a('khanifzyen','https://khanifzyen.my.id',['target'=>'_blank']) ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
