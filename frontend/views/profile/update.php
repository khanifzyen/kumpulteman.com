<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Profile',
]) . Yii::$app->user->identity->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->user->identity->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="panel panel-primary">
    <div class="panel-heading"><strong><?= Html::encode($this->title)?></strong></div>
        <div class="panel-body">
            <div class="profile-update">
            
                <?= $this->render('_form', [
                    'model' => $model,
                    'modelKelas'=>$modelKelas,
                ]) ?>
            
            </div>
        </div>
</div>
