kumpulteman.com
===============================

kumpulteman.com adalah platform yang dikhususkan bagi anda yang ingin bernostalgia dengan teman-teman sekolah.

Cara kolaborasi
- lakukan clone dari bitbucket dengan perintah git clone https://khanifzyen@bitbucket.org/khanifzyen/kumpulteman.com.git
- jalankan ./init dan pilih development
- edit konfigurasi database pada common/config/main-local.php
- tambahkan nilai 'tablePrefix' => 'tbl'
- jalankan ./yii migrate 1 untuk migrasi pertama yaite tbl_user
- signup terlebih dahulu untuk user admin, lewat frontend/web
- jalankan semua migrasi dengan ./yii migrate up
- selamat ngoding

