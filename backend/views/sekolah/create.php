<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Sekolah */

$this->title = Yii::t('app', 'Create Sekolah');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sekolahs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sekolah-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
