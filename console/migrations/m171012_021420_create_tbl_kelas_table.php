<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_kelas`.
 * Has foreign keys to the tables:
 *
 * - `tbl_sekolah`
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_021420_create_tbl_kelas_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_kelas', [
            'id' => $this->primaryKey(),
            'id_sekolah' => $this->integer()->notNull(),
            'nama' => $this->string(255)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `id_sekolah`
        $this->createIndex(
            'idx-tbl_kelas-id_sekolah',
            'tbl_kelas',
            'id_sekolah'
        );

        // add foreign key for table `tbl_sekolah`
        $this->addForeignKey(
            'fk-tbl_kelas-id_sekolah',
            'tbl_kelas',
            'id_sekolah',
            'tbl_sekolah',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_kelas-created_by',
            'tbl_kelas',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_kelas-created_by',
            'tbl_kelas',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_kelas-updated_by',
            'tbl_kelas',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_kelas-updated_by',
            'tbl_kelas',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
        
        //batch insert kelas untuk sman 1 jepara
        $date = new DateTime();
        $this->batchInsert('tbl_kelas',['id_sekolah','nama','created_at','updated_at','created_by','updated_by'],[
            [1,'XII Bahasa',$date->getTimestamp(),'',1,1],
            [1,'XII IPA 1',$date->getTimestamp(),'',1,1],
            [1,'XII IPA 2',$date->getTimestamp(),'',1,1],
            [1,'XII IPA 3',$date->getTimestamp(),'',1,1],
            [1,'XII IPA 4',$date->getTimestamp(),'',1,1],
            [1,'XII IPS 1',$date->getTimestamp(),'',1,1],
            [1,'XII IPS 2',$date->getTimestamp(),'',1,1],
            ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_sekolah`
        $this->dropForeignKey(
            'fk-tbl_kelas-id_sekolah',
            'tbl_kelas'
        );

        // drops index for column `id_sekolah`
        $this->dropIndex(
            'idx-tbl_kelas-id_sekolah',
            'tbl_kelas'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_kelas-created_by',
            'tbl_kelas'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_kelas-created_by',
            'tbl_kelas'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_kelas-updated_by',
            'tbl_kelas'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_kelas-updated_by',
            'tbl_kelas'
        );

        $this->dropTable('tbl_kelas');
    }
}
