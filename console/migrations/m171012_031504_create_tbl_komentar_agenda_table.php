<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_komentar_agenda`.
 * Has foreign keys to the tables:
 *
 * - `tbl_agenda`
 * - `tbl_user`
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_031504_create_tbl_komentar_agenda_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_komentar_agenda', [
            'id' => $this->primaryKey(),
            'id_agenda' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'isi' => $this->text()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `id_agenda`
        $this->createIndex(
            'idx-tbl_komentar_agenda-id_agenda',
            'tbl_komentar_agenda',
            'id_agenda'
        );

        // add foreign key for table `tbl_agenda`
        $this->addForeignKey(
            'fk-tbl_komentar_agenda-id_agenda',
            'tbl_komentar_agenda',
            'id_agenda',
            'tbl_agenda',
            'id',
            'CASCADE'
        );

        // creates index for column `id_user`
        $this->createIndex(
            'idx-tbl_komentar_agenda-id_user',
            'tbl_komentar_agenda',
            'id_user'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_komentar_agenda-id_user',
            'tbl_komentar_agenda',
            'id_user',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_komentar_agenda-created_by',
            'tbl_komentar_agenda',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_komentar_agenda-created_by',
            'tbl_komentar_agenda',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_komentar_agenda-updated_by',
            'tbl_komentar_agenda',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_komentar_agenda-updated_by',
            'tbl_komentar_agenda',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_agenda`
        $this->dropForeignKey(
            'fk-tbl_komentar_agenda-id_agenda',
            'tbl_komentar_agenda'
        );

        // drops index for column `id_agenda`
        $this->dropIndex(
            'idx-tbl_komentar_agenda-id_agenda',
            'tbl_komentar_agenda'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_komentar_agenda-id_user',
            'tbl_komentar_agenda'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            'idx-tbl_komentar_agenda-id_user',
            'tbl_komentar_agenda'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_komentar_agenda-created_by',
            'tbl_komentar_agenda'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_komentar_agenda-created_by',
            'tbl_komentar_agenda'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_komentar_agenda-updated_by',
            'tbl_komentar_agenda'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_komentar_agenda-updated_by',
            'tbl_komentar_agenda'
        );

        $this->dropTable('tbl_komentar_agenda');
    }
}
