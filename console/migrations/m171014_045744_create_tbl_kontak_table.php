<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_kontak`.
 */
class m171014_045744_create_tbl_kontak_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_kontak', [
            'id' => $this->primaryKey(),
            'nama' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'judul' => $this->string(255)->notNull(),
            'isi' => $this->text()->notNull(),
            'link_url' => $this->string(255),
            'file_capture' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tbl_kontak');
    }
}
