<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_agenda_peserta`.
 * Has foreign keys to the tables:
 *
 * - `tbl_user`
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_052313_create_tbl_agenda_peserta_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_agenda_peserta', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `id_user`
        $this->createIndex(
            'idx-tbl_agenda_peserta-id_user',
            'tbl_agenda_peserta',
            'id_user'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_agenda_peserta-id_user',
            'tbl_agenda_peserta',
            'id_user',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_agenda_peserta-created_by',
            'tbl_agenda_peserta',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_agenda_peserta-created_by',
            'tbl_agenda_peserta',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_agenda_peserta-updated_by',
            'tbl_agenda_peserta',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_agenda_peserta-updated_by',
            'tbl_agenda_peserta',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_agenda_peserta-id_user',
            'tbl_agenda_peserta'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            'idx-tbl_agenda_peserta-id_user',
            'tbl_agenda_peserta'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_agenda_peserta-created_by',
            'tbl_agenda_peserta'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_agenda_peserta-created_by',
            'tbl_agenda_peserta'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_agenda_peserta-updated_by',
            'tbl_agenda_peserta'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_agenda_peserta-updated_by',
            'tbl_agenda_peserta'
        );

        $this->dropTable('tbl_agenda_peserta');
    }
}
