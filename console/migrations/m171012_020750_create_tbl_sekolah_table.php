<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_sekolah`.
 * Has foreign keys to the tables:
 *
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_020750_create_tbl_sekolah_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_sekolah', [
            'id' => $this->primaryKey(),
            'nama' => $this->string(255)->notNull(),
            'alamat' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_sekolah-created_by',
            'tbl_sekolah',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_sekolah-created_by',
            'tbl_sekolah',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_sekolah-updated_by',
            'tbl_sekolah',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_sekolah-updated_by',
            'tbl_sekolah',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
        
         //insert sekolah sman 1 jepara
        $date = new DateTime();
        $this->insert('tbl_sekolah',['nama'=>'SMA N 1 Jepara','alamat'=>'','created_at'=>$date->getTimestamp(),'created_by'=>1,'updated_by'=>1]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_sekolah-created_by',
            'tbl_sekolah'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_sekolah-created_by',
            'tbl_sekolah'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_sekolah-updated_by',
            'tbl_sekolah'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_sekolah-updated_by',
            'tbl_sekolah'
        );

        $this->dropTable('tbl_sekolah');
    }
}
