<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_agenda`.
 * Has foreign keys to the tables:
 *
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_024722_create_tbl_agenda_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_agenda', [
            'id' => $this->primaryKey(),
            'judul' => $this->string(255)->notNull(),
            'isi' => $this->text()->notNull(),
            'tanggal_pelaksanaan' => $this->dateTime()->notNull(),
            'donasi' => $this->boolean()->notNull()->defaultValue(0),
            'terakhir_donasi' => $this->dateTime()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_agenda-created_by',
            'tbl_agenda',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_agenda-created_by',
            'tbl_agenda',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_agenda-updated_by',
            'tbl_agenda',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_agenda-updated_by',
            'tbl_agenda',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_agenda-created_by',
            'tbl_agenda'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_agenda-created_by',
            'tbl_agenda'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_agenda-updated_by',
            'tbl_agenda'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_agenda-updated_by',
            'tbl_agenda'
        );

        $this->dropTable('tbl_agenda');
    }
}
