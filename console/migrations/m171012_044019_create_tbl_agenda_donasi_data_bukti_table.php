<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_agenda_donasi_data_bukti`.
 * Has foreign keys to the tables:
 *
 * - `tbl_agenda_donasi_data`
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_044019_create_tbl_agenda_donasi_data_bukti_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_agenda_donasi_data_bukti', [
            'id' => $this->primaryKey(),
            'id_agenda_donasi_data' => $this->integer()->notNull(),
            'upload_bukti' => $this->string(255)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `id_agenda_donasi_data`
        $this->createIndex(
            'idx-tbl_agenda_donasi_data_bukti-id_agenda_donasi_data',
            'tbl_agenda_donasi_data_bukti',
            'id_agenda_donasi_data'
        );

        // add foreign key for table `tbl_agenda_donasi_data`
        $this->addForeignKey(
            'fk-tbl_agenda_donasi_data_bukti-id_agenda_donasi_data',
            'tbl_agenda_donasi_data_bukti',
            'id_agenda_donasi_data',
            'tbl_agenda_donasi_data',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_agenda_donasi_data_bukti-created_by',
            'tbl_agenda_donasi_data_bukti',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_agenda_donasi_data_bukti-created_by',
            'tbl_agenda_donasi_data_bukti',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_agenda_donasi_data_bukti-updated_by',
            'tbl_agenda_donasi_data_bukti',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_agenda_donasi_data_bukti-updated_by',
            'tbl_agenda_donasi_data_bukti',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_agenda_donasi_data`
        $this->dropForeignKey(
            'fk-tbl_agenda_donasi_data_bukti-id_agenda_donasi_data',
            'tbl_agenda_donasi_data_bukti'
        );

        // drops index for column `id_agenda_donasi_data`
        $this->dropIndex(
            'idx-tbl_agenda_donasi_data_bukti-id_agenda_donasi_data',
            'tbl_agenda_donasi_data_bukti'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_agenda_donasi_data_bukti-created_by',
            'tbl_agenda_donasi_data_bukti'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_agenda_donasi_data_bukti-created_by',
            'tbl_agenda_donasi_data_bukti'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_agenda_donasi_data_bukti-updated_by',
            'tbl_agenda_donasi_data_bukti'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_agenda_donasi_data_bukti-updated_by',
            'tbl_agenda_donasi_data_bukti'
        );

        $this->dropTable('tbl_agenda_donasi_data_bukti');
    }
}
