<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_foto_profile`.
 * Has foreign keys to the tables:
 *
 * - `tbl_profile`
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_023853_create_tbl_foto_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_foto_profile', [
            'id' => $this->primaryKey(),
            'id_profile' => $this->integer()->notNull(),
            'file_asli' => $this->string(255)->notNull(),
            'thumb_depan' => $this->string(255)->notNull(),
            'is_profile_pic' => $this->boolean()->notNull()->defaultValue(0),
            'like_this' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `id_profile`
        $this->createIndex(
            'idx-tbl_foto_profile-id_profile',
            'tbl_foto_profile',
            'id_profile'
        );

        // add foreign key for table `tbl_profile`
        $this->addForeignKey(
            'fk-tbl_foto_profile-id_profile',
            'tbl_foto_profile',
            'id_profile',
            'tbl_profile',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_foto_profile-created_by',
            'tbl_foto_profile',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_foto_profile-created_by',
            'tbl_foto_profile',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_foto_profile-updated_by',
            'tbl_foto_profile',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_foto_profile-updated_by',
            'tbl_foto_profile',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_profile`
        $this->dropForeignKey(
            'fk-tbl_foto_profile-id_profile',
            'tbl_foto_profile'
        );

        // drops index for column `id_profile`
        $this->dropIndex(
            'idx-tbl_foto_profile-id_profile',
            'tbl_foto_profile'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_foto_profile-created_by',
            'tbl_foto_profile'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_foto_profile-created_by',
            'tbl_foto_profile'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_foto_profile-updated_by',
            'tbl_foto_profile'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_foto_profile-updated_by',
            'tbl_foto_profile'
        );

        $this->dropTable('tbl_foto_profile');
    }
}
