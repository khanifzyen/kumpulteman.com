<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_berita`.
 * Has foreign keys to the tables:
 *
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_031734_create_tbl_berita_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_berita', [
            'id' => $this->primaryKey(),
            'judul' => $this->string(255)->notNull(),
            'isi' => $this->text()->notNull(),
            'gambar_berita' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_berita-created_by',
            'tbl_berita',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_berita-created_by',
            'tbl_berita',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_berita-updated_by',
            'tbl_berita',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_berita-updated_by',
            'tbl_berita',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_berita-created_by',
            'tbl_berita'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_berita-created_by',
            'tbl_berita'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_berita-updated_by',
            'tbl_berita'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_berita-updated_by',
            'tbl_berita'
        );

        $this->dropTable('tbl_berita');
    }
}
