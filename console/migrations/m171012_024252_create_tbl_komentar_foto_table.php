<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_komentar_foto`.
 * Has foreign keys to the tables:
 *
 * - `tbl_foto_profile`
 * - `tbl_user`
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_024252_create_tbl_komentar_foto_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_komentar_foto', [
            'id' => $this->primaryKey(),
            'id_foto_profile' => $this->integer()->notNull(),
            'isi' => $this->text()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `id_foto_profile`
        $this->createIndex(
            'idx-tbl_komentar_foto-id_foto_profile',
            'tbl_komentar_foto',
            'id_foto_profile'
        );

        // add foreign key for table `tbl_foto_profile`
        $this->addForeignKey(
            'fk-tbl_komentar_foto-id_foto_profile',
            'tbl_komentar_foto',
            'id_foto_profile',
            'tbl_foto_profile',
            'id',
            'CASCADE'
        );

        // creates index for column `id_user`
        $this->createIndex(
            'idx-tbl_komentar_foto-id_user',
            'tbl_komentar_foto',
            'id_user'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_komentar_foto-id_user',
            'tbl_komentar_foto',
            'id_user',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_komentar_foto-created_by',
            'tbl_komentar_foto',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_komentar_foto-created_by',
            'tbl_komentar_foto',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_komentar_foto-updated_by',
            'tbl_komentar_foto',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_komentar_foto-updated_by',
            'tbl_komentar_foto',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_foto_profile`
        $this->dropForeignKey(
            'fk-tbl_komentar_foto-id_foto_profile',
            'tbl_komentar_foto'
        );

        // drops index for column `id_foto_profile`
        $this->dropIndex(
            'idx-tbl_komentar_foto-id_foto_profile',
            'tbl_komentar_foto'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_komentar_foto-id_user',
            'tbl_komentar_foto'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            'idx-tbl_komentar_foto-id_user',
            'tbl_komentar_foto'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_komentar_foto-created_by',
            'tbl_komentar_foto'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_komentar_foto-created_by',
            'tbl_komentar_foto'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_komentar_foto-updated_by',
            'tbl_komentar_foto'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_komentar_foto-updated_by',
            'tbl_komentar_foto'
        );

        $this->dropTable('tbl_komentar_foto');
    }
}
