<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_profile`.
 * Has foreign keys to the tables:
 *
 * - `tbl_user`
 * - `tbl_kelas`
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_023829_create_tbl_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_profile', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'nama_panggilan' => $this->string(255),
            'nama_lengkap' => $this->string(255),
            'alamat_asal' => $this->string(255),
            'alamat_sekarang' => $this->string(255),
            'angkatan' => $this->integer(),
            'id_kelas' => $this->integer(),
            'pesan_kesan' => $this->text(),
            'pekerjaan' => $this->string(255),
            'no_hp_wa' => $this->string(20),
            'facebook' => $this->string(255),
            'instragram' => $this->string(255),
            'twitter' => $this->string(255),
            'path' => $this->string(255),
            'youtube' => $this->string(255),
            'folder_foto' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `id_user`
        $this->createIndex(
            'idx-tbl_profile-id_user',
            'tbl_profile',
            'id_user'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_profile-id_user',
            'tbl_profile',
            'id_user',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `id_kelas`
        $this->createIndex(
            'idx-tbl_profile-id_kelas',
            'tbl_profile',
            'id_kelas'
        );

        // add foreign key for table `tbl_kelas`
        $this->addForeignKey(
            'fk-tbl_profile-id_kelas',
            'tbl_profile',
            'id_kelas',
            'tbl_kelas',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_profile-created_by',
            'tbl_profile',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_profile-created_by',
            'tbl_profile',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_profile-updated_by',
            'tbl_profile',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_profile-updated_by',
            'tbl_profile',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_profile-id_user',
            'tbl_profile'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            'idx-tbl_profile-id_user',
            'tbl_profile'
        );

        // drops foreign key for table `tbl_kelas`
        $this->dropForeignKey(
            'fk-tbl_profile-id_kelas',
            'tbl_profile'
        );

        // drops index for column `id_kelas`
        $this->dropIndex(
            'idx-tbl_profile-id_kelas',
            'tbl_profile'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_profile-created_by',
            'tbl_profile'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_profile-created_by',
            'tbl_profile'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_profile-updated_by',
            'tbl_profile'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_profile-updated_by',
            'tbl_profile'
        );

        $this->dropTable('tbl_profile');
    }
}
