<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tbl_agenda_donasi_data`.
 * Has foreign keys to the tables:
 *
 * - `tbl_agenda`
 * - `tbl_user`
 * - `tbl_user`
 * - `tbl_user`
 */
class m171012_030446_create_tbl_agenda_donasi_data_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tbl_agenda_donasi_data', [
            'id' => $this->primaryKey(),
            'id_agenda' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'nominal' => $this->bigInteger()->notNull()->defaultValue(0),
            'validasi' => $this->boolean()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `id_agenda`
        $this->createIndex(
            'idx-tbl_agenda_donasi_data-id_agenda',
            'tbl_agenda_donasi_data',
            'id_agenda'
        );

        // add foreign key for table `tbl_agenda`
        $this->addForeignKey(
            'fk-tbl_agenda_donasi_data-id_agenda',
            'tbl_agenda_donasi_data',
            'id_agenda',
            'tbl_agenda',
            'id',
            'CASCADE'
        );

        // creates index for column `id_user`
        $this->createIndex(
            'idx-tbl_agenda_donasi_data-id_user',
            'tbl_agenda_donasi_data',
            'id_user'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_agenda_donasi_data-id_user',
            'tbl_agenda_donasi_data',
            'id_user',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            'idx-tbl_agenda_donasi_data-created_by',
            'tbl_agenda_donasi_data',
            'created_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_agenda_donasi_data-created_by',
            'tbl_agenda_donasi_data',
            'created_by',
            'tbl_user',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-tbl_agenda_donasi_data-updated_by',
            'tbl_agenda_donasi_data',
            'updated_by'
        );

        // add foreign key for table `tbl_user`
        $this->addForeignKey(
            'fk-tbl_agenda_donasi_data-updated_by',
            'tbl_agenda_donasi_data',
            'updated_by',
            'tbl_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tbl_agenda`
        $this->dropForeignKey(
            'fk-tbl_agenda_donasi_data-id_agenda',
            'tbl_agenda_donasi_data'
        );

        // drops index for column `id_agenda`
        $this->dropIndex(
            'idx-tbl_agenda_donasi_data-id_agenda',
            'tbl_agenda_donasi_data'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_agenda_donasi_data-id_user',
            'tbl_agenda_donasi_data'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            'idx-tbl_agenda_donasi_data-id_user',
            'tbl_agenda_donasi_data'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_agenda_donasi_data-created_by',
            'tbl_agenda_donasi_data'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-tbl_agenda_donasi_data-created_by',
            'tbl_agenda_donasi_data'
        );

        // drops foreign key for table `tbl_user`
        $this->dropForeignKey(
            'fk-tbl_agenda_donasi_data-updated_by',
            'tbl_agenda_donasi_data'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-tbl_agenda_donasi_data-updated_by',
            'tbl_agenda_donasi_data'
        );

        $this->dropTable('tbl_agenda_donasi_data');
    }
}
